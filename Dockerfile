FROM registry.access.redhat.com/ubi8/ubi:8.0
RUN yum -y install wget git && \
    mkdir -p example
WORKDIR example/    
RUN wget -O gitea https://dl.gitea.io/gitea/1.14.2/gitea-1.14.2-linux-amd64 && \  
    chmod +x gitea && \
    mkdir -p /var/lib/gitea/{custom,data,log} && \
    chmod -R 750 /var/lib/gitea/ && \
    mkdir /etc/gitea  && \
    chmod 750 /etc/gitea
ENV GITEA_WORK_DIR=/var/lib/gitea/
RUN cp gitea /usr/local/bin/gitea
#RUN GITEA_WORK_DIR=/var/lib/gitea/ /usr/local/bin/gitea web -c /etc/gitea/app.ini
#RUN GITEA_WORK_DIR=/var/lib/gitea/ /usr/local/bin/gitea web
COPY app.ini /etc/gitea/custom/conf/app.ini
EXPOSE 3000
CMD /usr/local/bin/gitea web -c /etc/gitea/custom/conf/app.ini

